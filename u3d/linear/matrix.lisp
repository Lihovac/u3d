(import lua/basic (get-idx))

(defun index (x y width height)
  "Convert coordinates X, Y into an index in a WIDTH*HEIGHT matrix."
  :hidden
  (+ (* (pred y) width) x))

(defmacro define-matrix (name width height)
  "Create a matrix constructor with the given WIDTH and HEIGHT."
  (assert-type! name symbol)
  (assert-type! width number)
  (assert-type! height number)

  (let [(args '())
        (width (const-val width))
        (height (const-val height))

        (mk-vals '())
        (id-vals '())
        (pretty-vals '())
        (mul-vals '())
        (trans-vals '())]
    (for y 1 height 1
      (when (> y 1) (push! pretty-vals "\n "))
      (for x 1 width 1
        (with (arg (gensym (.. "m" width "-" height)))
          (push! args arg)

          (push! mk-vals (index x y width height))
          (push! mk-vals (list `unquote arg)))

        (push! id-vals (index x y width height))
        (push! id-vals (if (= x y) 1 0))

        (when (> x 1) (push! pretty-vals " "))
        (push! pretty-vals `(get-idx ,'x ,(index x y width height)))

        (push! mul-vals (index x y width height))
        (push! mul-vals `(* (get-idx ,'x ,(index x y width height)) ,'y))

        (push! trans-vals (index x y width height))
        (push! trans-vals `(get-idx ,'x ,(index y x width height)))
        ))

    (values-list
      `(defmacro ,name ,args
         `{ :tag ,,(symbol->string name) ,@,mk-vals })
      `(defmethod (pretty ,name) (,'x)
         (.. "[" ,@pretty-vals "]"))
      `(define ,(sym.. name 'i)
         { :tag ,(symbol->string name) ,@id-vals })
      `(defun ,(sym.. name '*) (,'x ,'y)
         (assert-type! ,'x ,name)
         (assert-type! ,'y ,'number)
         { :tag ,(symbol->string name) ,@mul-vals })
      `(defun ,(sym.. name 't) (,'x)
         { :tag ,(symbol->string name) ,@trans-vals })
       )))

(defmacro define-matrix-mul (name res-name
                             left-name left-width left-height
                             right-name right-width right-height)
  (assert-type! name symbol)
  (assert-type! res-name symbol)

  (assert-type! left-name symbol)
  (assert-type! left-width number)
  (assert-type! left-height number)

  (assert-type! right-name symbol)
  (assert-type! right-width number)
  (assert-type! right-height number)

  (demand (eq? left-width right-height))

  (let [(binds '())
        (left-vars '())
        (right-vars '())
        (out '())

        (left-width (const-val left-width))
        (left-height (const-val left-height))
        (right-width (const-val right-width))
        (right-height (const-val right-height))]

    (for y 1 left-height 1
      (for x 1 left-width 1
        (with (var (gensym (.. "l" x "-" y)))
          (push! left-vars var)
          (push! binds `(,var (get-idx ,'left ,(index x y left-width left-height)))))))

    (for y 1 right-height 1
      (for x 1 right-width 1
        (with (var (gensym (.. "r" x "-" y)))
          (push! right-vars var)
          (push! binds `(,var (get-idx ,'right ,(index x y right-width right-height)))))))

    (for y 1 left-height 1
      (for x 1 right-width 1
        (with (row `(+))
          (for i 1 left-width 1
            (push! row `(* ,(.> left-vars (index i y left-width left-height))
                           ,(.> right-vars (index x i right-width right-height)))))
          (push! out (index x y right-width left-height))
          (push! out row))))

    `(defun ,name (,'left ,'right)
       (assert-type! ,'left ,left-name)
       (assert-type! ,'right ,right-name)
       (let ,binds
         { :tag ,(symbol->string res-name) ,@out }))))
