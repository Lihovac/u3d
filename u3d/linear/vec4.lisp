(import lua/basic (get-idx))

(import u3d/linear/matrix ())

(define-native m :hidden :syntax "-${1}")

(define-matrix m4 4 4)
(define-matrix v4 1 4)

(define-matrix-mul m4*m4
  m4
  m4 4 4
  m4 4 4)

(define-matrix-mul m4*v4
  v4
  m4 4 4
  v4 1 4)

(defmacro coord (vector axis)
  "Extract an AXIS from the given VECTOR. This does not do any type
   checking (it is just a helper for low-level access) so you should be
   careful with it."
  (case axis
    [:x `(get-idx ,vector 1)]
    [:y `(get-idx ,vector 2)]
    [:z `(get-idx ,vector 3)]
    [:w `(get-idx ,vector 4)]))

(defun translate (x y z)
  "Create a transformation matrix which moves objects by the vector
   (X, Y, Z)."
  (assert-type! x number)
  (assert-type! y number)
  (assert-type! z number)

  (m4 1 0 0 x
      0 1 0 y
      0 0 1 z
      0 0 0 1))

(defun scale (x y z)
  "Create a transformation matrix which scales objects by the vector
   (X, Y, Z)."
  (assert-type! x number)
  (assert-type! y number)
  (assert-type! z number)

  (m4 x 0 0 0
      0 y 0 0
      0 0 z 0
      0 0 0 1))

(defun rot-x (angle)
  "Create a transformation matrix which rotates objects on the x axis by
   the given ANGLE."
  (let [(c (math/cos angle))
        (s (math/sin angle))]
     (m4 1 0    0  0
         0 c (m s) 0
         0 s    c  0
         0 0    0  1)))

(defun rot-y (angle)
  "Create a transformation matrix which rotates objects on the y axis by
   the given ANGLE."
  (let [(c (math/cos angle))
        (s (math/sin angle))]
     (m4 c     0 s 0
         0     1 0 0
         (m s) 0 c 0
         0     0 0 1)))

(defun rot-z (angle)
  "Create a transformation matrix which rotates objects on the z axis by
   the given ANGLE."
  (let [(c (math/cos angle))
        (s (math/sin angle))]
     (m4 c (m s) 0 0
         s    c  0 0
         s    0  1 0
         0    0  0 1)))

(defun perspective (fov aspect z-near z-far)
  "Create a perspective projection matrix, with the given FOV on the y
   axis, ASPECT ratio and Z-NEAR and Z-FAR fields."
  (with (tan-half (math/tan (* fov 0.5)))
    (m4 (/ 1 (* aspect tan-half)) 0                0                                     0
        0                         (/ 1 tan-half )  0                                     0
        0                         0                (/ (+ z-near z-far) (- z-near z-far)) (/ (* 2 z-near z-far) (- z-near z-far))
        0                         0                -1                                    0)))
